'use strict';

var config = require('./config'),
  express = require('express'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE');
  res.setHeader('Access-Control-Max-Age', '60');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
});

var accesoRouter = require('./proyecto/routes/acceso.router'),
  estudianteRouter = require('./proyecto/routes/estudiante.router'),
  novedadesRouter = require('./proyecto/routes/novedades.router'),
  notificacionesRouter = require('./proyecto/routes/notificaciones.router')
;

app.use('/apptui/acceso/', accesoRouter);
app.use('/apptui/estudiante/', estudianteRouter);
app.use('/apptui/novedades/', novedadesRouter);
app.use('/apptui/notificaciones/', notificacionesRouter);

module.exports = app;